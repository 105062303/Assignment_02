var input2State = {
    create: function () {
        game.add.image(0, 0, 'star');
        bool = -1;

        var nameLabel = game.add.text(game.width / 2, -50, 'Tap to Start',
            { font: '40px Arial', fill: '#ffffff' });
        nameLabel.anchor.setTo(0.5, 0.5);
        game.add.tween(nameLabel).to({ y: 80 }, 1000).easing(Phaser.Easing.Bounce.Out).start();
        game.add.plugin(Fabrique.Plugins.InputField);

        this.Label1 = game.add.text(game.width / 2, 200,
            'Player1: ', { font: '25px Arial', fill: '#ffffff' });
        this.Label1.anchor.setTo(0.5, 0.5);
        game.add.tween(this.Label1).to({ x: 100.0, y: 150.0 }, 2400, Phaser.Easing.Bounce.Out, true);

        this.player = game.add.sprite(100, 200, 'player');
        game.physics.arcade.enable(this.player);

        this.Label3 = game.add.text(game.width / 2, 200,
            '←,→ to control ', { font: '25px Arial', fill: '#ffffff' });
        this.Label3.anchor.setTo(0.5, 0.5);
        game.add.tween(this.Label3).to({ x: 100.0, y: 270.0 }, 2400, Phaser.Easing.Bounce.Out, true);

        this.Label2 = game.add.text(game.width / 2, 200,
            'Player2: ', { font: '25px Arial', fill: '#ffffff' });
        this.Label2.anchor.setTo(0.5, 0.5);
        game.add.tween(this.Label2).to({ x: 300.0, y: 150.0 }, 2400, Phaser.Easing.Bounce.Out, true);

        this.Label4 = game.add.text(game.width / 2, 200,
            'A,D to control ', { font: '25px Arial', fill: '#ffffff' });
        this.Label4.anchor.setTo(0.5, 0.5);
        game.add.tween(this.Label4).to({ x: 300.0, y: 270.0 }, 2400, Phaser.Easing.Bounce.Out, true);

        this.player2 = game.add.sprite(300, 200, 'player2');
        game.physics.arcade.enable(this.player2);



        this.startLabel = game.add.text(game.width / 2, 200,
            'press the up arrow key to start', { font: '25px Arial', fill: '#ffffff' });
        this.startLabel.anchor.setTo(0.5, 0.5);
        game.add.tween(this.startLabel).to({ x: 200.0, y: 350.0 }, 2400, Phaser.Easing.Bounce.Out, true);


        var upKey = game.input.keyboard.addKey(Phaser.Keyboard.UP);

        upKey.onDown.add(this.start, this);

    },
    update: function () {

        this.startLabel.visible = true;


    },
    start: function () {
        game.state.start('play2');
    },
}