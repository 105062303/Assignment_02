var inputState={
    create: function(){
        game.add.image(0, 0, 'star');
        bool=-1;

        var nameLabel = game.add.text(game.width/2, -50, 'Enter Player Name',
        { font: '40px Arial', fill: '#ffffff' }); 
        nameLabel.anchor.setTo(0.5, 0.5);
        game.add.tween(nameLabel).to({y: 80}, 1000).easing(Phaser.Easing.Bounce.Out).start();
        game.add.plugin(Fabrique.Plugins.InputField);

        input = game.add.inputField(100, game.height/2-30, {
            font: '18px Arial',
            fill: '#212121',
            fontWeight: '500',
            width: 150,
            padding: 8,
            borderWidth: 1,
            borderColor: '#99ff99',
            borderRadius: 6,
            placeHolder: 'your name',
            //type: Fabrique.InputType.password
        });

        var submitBtn = game.add.image(280, game.height/2-30, 'btn');

        //submitBtn.animations.add('up', [0, 1, 2, 3, 4, 5], 8, true);
        //submitBtn.animations.add('down', [6], 8, true);
        
        //var submit = game.add.text(game.width / 2 - 80, 380, 'Submit', {
        //    font: '18px Arial'
        //});
        /*submitBtn.animations.play('up');*/
        submitBtn.inputEnabled = true;
        submitBtn.input.useHandCursor = true;
        submitBtn.events.onInputDown.add(function() {   
            if(input.value!=""){
                //submitBtn.animations.play('down');
                bool=1;
            }
            else
                bool=-1;

        });



        this.name=input.value;
        this.startLabel = game.add.text(game.width/2, 200,
        'press the up arrow key to start', { font: '25px Arial', fill: '#ffffff' }); 
        this.startLabel.anchor.setTo(0.5, 0.5);
        game.add.tween(this.startLabel).to({x: 200.0, y: 350.0}, 2400, Phaser.Easing.Bounce.Out, true);


        var upKey = game.input.keyboard.addKey(Phaser.Keyboard.UP);
        
        upKey.onDown.add(this.start, this); 
    
    },
    update: function(){
        if(input.value=="") 
            this.startLabel.visible=false;
        else {
            if(bool==1)
                this.startLabel.visible=true;
        }

    },
    start: function(){
            game.state.start('play');
    },
}