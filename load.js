var loadState = {
    preload: function () {
        // Add a 'loading...' label on the screen
        var loadingLabel = game.add.text(game.width / 2, 150,
            'loading...', { font: '30px Arial', fill: '#ffffff' });
        loadingLabel.anchor.setTo(0.5, 0.5);
        // Display the progress bar
        var progressBar = game.add.sprite(game.width / 2, 200, 'progressBar');
        progressBar.anchor.setTo(0.5, 0.5);
        game.load.setPreloadSprite(progressBar);
        // Load all game assets
        game.load.spritesheet('player', './assets/player.png', 32, 32);
        game.load.spritesheet('player2', './assets/MARIO.png', 32, 48);
        game.load.image('normal', './assets/normal.png');
        game.load.image('nails', './assets/nails.png');
        game.load.image('star', 'assets/star.jpg');
        game.load.image('bg', 'assets/bg.jpg');
        game.load.spritesheet('conveyorRight', './assets/conveyor_right.png', 96, 16);
        game.load.spritesheet('conveyorLeft', './assets/conveyor_left.png', 96, 16);
        game.load.spritesheet('trampoline', './assets/trampoline.png', 96, 22);
        game.load.spritesheet('fake', './assets/fake.png', 96, 36);
        game.load.image('wall', './assets/wall.png');
        game.load.image('ceiling', './assets/ceiling.png');
        game.load.audio('hit', 'assets/sounds/hit.mp3');
        game.load.audio('jump', 'assets/sounds/jump.ogg');
        game.load.audio('scream', 'assets/sounds/scream.mp3');
        game.load.image('pixel', 'assets/pixel.png');
        game.load.image('btn', './assets/button.png');
        // Load a new asset that we will use in the menu state
        //game.load.image('background', 'assets/background.jpg');
    },
    create: function () {
        // Go to the menu state
        game.state.start('menu');
    }
}; 