var menuState = {
    create: function () {
        // Add a background image
        //game.add.image(0, 0, 'background');
        game.add.image(0, 0, 'star');
        // Display the name of the game
        var nameLabel = game.add.text(game.width / 2, 10, 'NS-SHAFT',
            { font: '50px Arial', fill: '#ffffff' });
        nameLabel.anchor.setTo(0.5, 0.5);
        game.add.tween(nameLabel).to({x: 200.0, y: 80.0}, 2400, Phaser.Easing.Bounce.Out, true);
        // Show the score at the center of the screen
        var scoreLabel = game.add.text(game.width / 2, 30,
            'score: ' + game.global.score, { font: '25px Arial', fill: '#ffffff' });
        scoreLabel.anchor.setTo(0.5, 0.5);
        game.add.tween(scoreLabel).to({x: 200.0, y: 190.0}, 2400, Phaser.Easing.Bounce.Out, true);

        var ranking = game.add.text(200, 60, 'rankings', { font: '25px Arial', fill: '#ffffff' });
        game.add.tween(ranking).to({x: 200.0, y: 230.0}, 2400, Phaser.Easing.Bounce.Out, true);
        ranking.anchor.setTo(0.5, 0.5);
        ranking.inputEnabled = true;
        ranking.input.useHandCursor = true;
        ranking.events.onInputDown.add(function() {
            window.location="leaderboard.html";
        });
        // Explain how to start the game
        var startLabel = game.add.text(game.width / 2, 50,
            'press the up arrow key to start', { font: '25px Arial', fill: '#ffffff' });
        startLabel.anchor.setTo(0.5, 0.5);
        game.add.tween(startLabel).to({x: 200.0, y: 320.0}, 2400, Phaser.Easing.Bounce.Out, true);
        var startLabel1 = game.add.text(game.width/2, 70,
            'the down arrow key for 2p', { font: '25px Arial', fill: '#ffffff' }); 
            startLabel1.anchor.setTo(0.5, 0.5);
            game.add.tween(startLabel1).to({x: 200.0, y: 350.0}, 2400, Phaser.Easing.Bounce.Out, true);
        /*var startLabel = game.add.text(game.width/2, game.height-100,
            'the up arrow key for single', { font: '25px Arial', fill: '#ffffff' }); 
            startLabel.anchor.setTo(0.5, 0.5);
        game.add.tween(startLabel).to({angle: -2}, 500).to({angle: 2}, 1000).to({angle: 0}, 500).loop().start();*/
        // Create a new Phaser keyboard variable: the up arrow key
        // When pressed, call the 'start'
        var upKey = game.input.keyboard.addKey(Phaser.Keyboard.UP);
        upKey.onDown.add(this.start, this);
        var downKey = game.input.keyboard.addKey(Phaser.Keyboard.DOWN);
        downKey.onDown.add(this.start2, this);
    
    
        },
        start: function() {
        // Start the actual game 
        game.state.start('input');
        }, 
        start2: function(){
            game.state.start('input2');
        },
}; 